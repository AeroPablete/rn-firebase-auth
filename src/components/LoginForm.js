import firebase from 'firebase';
import React, { Component } from 'react';
import { Text } from 'react-native';
import { Button, Card, CardSection, Input, Spinner } from './common';

export default class LoginForm extends Component {
    state = {
        email: '', 
        password: '', 
        authError: '',
        loading: false
    }

    /**
     * 
     */
    onButtonPress() {
        const {email, password} = this.state;
        this.setState({authError: '', loading: true});

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(this.onAuthSucess.bind(this))
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then(this.onAuthSucess.bind(this))
                    .catch(this.onAuthFailed.bind(this))
            })
    }

    /**
     * 
     */
    onAuthSucess() {
        this.setState({email: '', password: '', authError: '', loading: false});
    }

    /**
     * 
     */
    onAuthFailed() {
        this.setState({authError: 'Authentication failed', loading: false});
    }    

    /**
     * 
     */
    render() {
        return (
            <Card>
                <CardSection>
                    <Input 
                        label="Email" 
                        placeholder="user@email.com"
                        value={ this.state.email }
                        onChangeText={ email => this.setState({ email }) } />
                </CardSection>

                <CardSection>
                    <Input 
                        label="Password" 
                        placeholder="password"
                        secureTextEntry
                        value={ this.state.password }
                        onChangeText={ password => this.setState({ password }) } />
                </CardSection>

                <Text style={styles.authErrorStyle}>
                    { this.state.authError }
                </Text>

                <CardSection>
                    { this.renderButton() }
                </CardSection>
            </Card>
        )
    }

    /**
     * 
     */
    renderButton() {
        if (this.state.loading)
            return <Spinner size="small" />;
        
        return (
            <Button onPress={this.onButtonPress.bind(this)}>
                Log In
            </Button>
        );
    }
}

const styles = {
    authErrorStyle: {
        alignSelf: 'center',
        color: 'red',
        fontSize: 20
    }
}
