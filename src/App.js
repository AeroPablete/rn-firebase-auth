import firebase from 'firebase';
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Button, Header, Spinner } from './components/common';
import LoginForm from './components/LoginForm';

export default class App extends Component {
    state = {loggedIn: null};

    /**
     * 
     */
    componentWillMount() {
        firebase.initializeApp({
            apiKey: "AIzaSyBA40c57bKrYqv_TZl1SLOvsA5j5DK0HLs",
            authDomain: "rn-firebase-auth-13f85.firebaseapp.com",
            databaseURL: "https://rn-firebase-auth-13f85.firebaseio.com",
            projectId: "rn-firebase-auth-13f85",
            storageBucket: "rn-firebase-auth-13f85.appspot.com",
            messagingSenderId: "766712047944"
        });   
        
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.setState({loggedIn: true});
            }
            else {
                this.setState({loggedIn: false});
            }
        });
    }

    /**
     * 
     */
    renderContent() {
        switch(this.state.loggedIn) {
            case true:
                return (
                    <View style={{marginTop: 60}}>
                        <Button
                            onPress={() => firebase.auth().signOut()} 
                        >Log Out</Button>
                    </View>
                )
            case false:
                return <LoginForm />
            default:
                return (
                    <View style={{marginTop: 60}}>
                        <Spinner />
                    </View>
                )
        }        
    }

    /**
     * 
     */
    render() {
        return (
            <View>
                <Header headerText="Authentication" />
                { this.renderContent() }
            </View>
        );
    }    
}